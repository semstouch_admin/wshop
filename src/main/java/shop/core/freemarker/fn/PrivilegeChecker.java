package shop.core.freemarker.fn;

import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import shop.core.util.PrivilegeUtil;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 检验权限码
 */
public class PrivilegeChecker implements TemplateMethodModelEx {
    private static Logger logger = LoggerFactory.getLogger(PrivilegeChecker.class);
    @Autowired
    private HttpSession session;
    @Override
    public Object exec(List arguments) throws TemplateModelException {
        if(arguments == null || arguments.size() == 0){
            return true;
        }
        if(!(arguments.get(0) instanceof String)){
            return true;
        }
        String res = (String)arguments.get(0);
        if(StringUtils.isBlank(res)){
            return true;
        }
        logger.info("check privilege ,res : {}, session id :{}", res, session == null ? null : session.getId());
        return PrivilegeUtil.check(session, res);
    }
}
