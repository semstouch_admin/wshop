package shop.core.common.oscache;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import shop.core.cache.CacheProvider;
import shop.core.cache.SimpleCacheProvider;
import shop.core.common.bean.AccessToken;
import shop.core.common.listener.SystemListener;
import shop.services.front.area.bean.Area;
import shop.services.manage.systemSetting.bean.SystemSetting;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Properties;


/**
 * 系统管理类.
 * 1、负责管理system.properties的东东
 * 2、负责缓存管理
 *
 * @author huangf
 */
public class SystemManager {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SystemManager.class);
    private static Properties p = new Properties();
    private static CacheProvider cacheProvider = new SimpleCacheProvider();
    private static SystemManager instance;

    @PostConstruct
    public void afterPropertiesSet() {
        instance = this;
    }

    public static SystemManager getInstance() {
        return instance;
    }

    static {
        init();
    }

    private static void init() {
        try {
            p.load(SystemListener.class.getResourceAsStream("/system.properties"));
            logger.info(p.toString());
        } catch (IOException e) {
            logger.error(e.toString());
        }
    }

    public String getProperty(String key) {
        return p.getProperty(key);
    }


    //应用缓存管理

    public void setCacheProvider(CacheProvider cacheProvider) {
        SystemManager.cacheProvider = cacheProvider;
    }

    private static String buildKey(String key) {
        return "SystemManager." + StringUtils.trimToEmpty(key);
    }

    private static void putCacheObject(String key, Serializable cacheObject) {
        String key1 = buildKey(key);
        if (cacheObject == null) {
            cacheProvider.remove(key1);
        } else {
            cacheProvider.put(key1, cacheObject);
        }
    }

    private static <T extends Serializable> T getCacheObject(String key) {
        return (T) cacheProvider.get(buildKey(key));
    }


    //系统设置
    public SystemSetting getSystemSetting() {
        return getCacheObject("systemSetting");
    }

    public void setSystemSetting(SystemSetting systemSetting) {
        putCacheObject("systemSetting", systemSetting);
    }



    /**
     * 省市区集合
     *
     * @return
     */
    public Map<String, Area> getAreaMap() {
        return getCacheObject("areaMap");
    }

    public void setAreaMap(Map<String, Area> areaMap) {
        putCacheObject("areaMap", (Serializable) areaMap);
    }

    /**
     * 城市列表
     *
     * @return
     */
    public List<Area> getCityList() {
        return getCacheObject("cityAllList");
    }

    public void setCityList(List<Area> cityList) {
        putCacheObject("cityAllList", (Serializable) cityList);
    }


    /**
     * 微信token
     * @return
     */
    public AccessToken getAccessToken() {
        return getCacheObject("accessToken");
    }

    public void setAccessToken(AccessToken accessToken) {
        putCacheObject("accessToken", (Serializable)accessToken);
    }


    /**
     * 微信jsapiTicket
     * @return
     */
    public String getJsapiTicket() {
        return getCacheObject("jsapiTicket");
    }

    public void setJsapiTicket(String jsapiTicket) {
        putCacheObject("jsapiTicket", (Serializable)jsapiTicket);
    }


}
