package shop.core.common.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/3/11.
 */
public class JSONResult<T> implements Serializable {
    private T data;
    private String message;

    private int statusCode;

    private boolean success;
    public  JSONResult(){
        data=null;
        message="success";
        statusCode=0;
        success=true;
    }
    public  JSONResult(T data){
        this.data=data;
    }

    public  JSONResult(T data,boolean success){
        this.data=data;
        this.success=success;
    }
    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
