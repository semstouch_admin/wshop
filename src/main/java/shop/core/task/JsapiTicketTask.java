package shop.core.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import shop.core.common.oscache.ManageCache;

/**
 * @Description 获取微信公众号jsapi_ticket 参考https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141115
 * @Author semstouch
 * @Date 2017/7/30
 **/
@Component
public class JsapiTicketTask implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(JsapiTicketTask.class);

    @Autowired
    private ManageCache manageCache;

    @Override
    public void run() {
        logger.debug("定时获取微信JsapiTicket!");
        manageCache.loadJsapiTicket();
    }
}
