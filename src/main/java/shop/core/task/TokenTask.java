package shop.core.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import shop.core.common.oscache.ManageCache;

/**
 * @Description 微信token刷新定时任务
 * @Author semstouch
 * @Date 2017/5/20
 **/
@Component
public class TokenTask implements Runnable{
    private static final Logger logger = LoggerFactory.getLogger(TokenTask.class);

    @Autowired
    private ManageCache manageCache;

    public void setManageCache(ManageCache manageCache) {
        this.manageCache = manageCache;
    }

    @Override
    public void run() {
        logger.debug("定时获取微信AccessToken!");
        manageCache.loadAccessToken();
    }
}
