package shop.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import shop.core.common.bean.JSONResult;
import shop.core.common.dao.page.PagerModel;
import shop.core.common.oscache.SystemManager;
import shop.services.front.account.bean.Account;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 前台Controller抽象类
 *
 * @author lintz
 */
@Controller
public abstract class FrontBaseController<E extends PagerModel> {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    public abstract Services<E> getService();

    @Autowired
    protected SystemManager systemManager;

    protected JSONResult jsonResult;


    /**
     * 分页查询方法
     *
     * @param request 请求
     * @param e       对象参数
     * @return 分页数据模型
     */
    @RequestMapping("/selectPageList")
    @ResponseBody
    public PagerModel selectPageList(HttpServletRequest request, E e) {
        int offset = 0;
        int pageSize = 10;
        if (request.getParameter("start") != null) {
            offset = Integer.parseInt(request.getParameter("start"));
        }
        if (request.getParameter("length") != null) {
            pageSize = Integer.parseInt(request.getParameter("length"));
        }
        if (offset < 0)
            offset = 0;
        if (pageSize < 0) {
            pageSize = 10;
        }
        e.setOffset((offset-1)*pageSize);
        e.setPageSize(pageSize);
        PagerModel pager = getService().selectPageList(e);
        pager.setRecordsTotal(pager.getTotal());
        pager.setRecordsFiltered(pager.getTotal());
        return pager;
    }
    /**
     * 查询全部方法
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/selectAllList")
    @ResponseBody
    public JSONResult selectAllList(HttpServletRequest request, @ModelAttribute("e") E e) throws Exception {
        List<E> rsList = getService().selectList(e);
        jsonResult = new JSONResult();
        jsonResult.setData(rsList);
        return jsonResult;
    }
    /**
     * 根据属性查询单个方法
     * @param e
     * @return
     */
    @RequestMapping(value = "/selectDetail", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult selectDetail(@ModelAttribute("e") E e) {
        e =  getService().selectOne(e);
        jsonResult = new JSONResult();
        jsonResult.setData(e);
        return jsonResult;
    }

    /**
     * 插入方法，子类可以通过重写此方法实现个性化的需求。
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/insertJson", method = RequestMethod.POST)
     @ResponseBody
     public JSONResult insertJson(HttpServletRequest request, @ModelAttribute("e") E e) throws Exception {
        jsonResult = new JSONResult();
        getService().insert(e);
        return jsonResult;
    }

    /**
     * 公共的更新数据的方法，子类可以通过重写此方法实现个性化的需求。
     *
     * @return jsonResult
     * @throws Exception
     */
    @RequestMapping(value = "/updateJson", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult updateJson(HttpServletRequest request, @ModelAttribute("e") E e) throws Exception {
        jsonResult = new JSONResult();
        getService().update(e);
        return jsonResult;
    }

}
