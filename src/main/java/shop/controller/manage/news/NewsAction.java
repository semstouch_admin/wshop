/**
 * 2012-7-8
 * jqsl2012@163.com
 */
package shop.controller.manage.news;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import shop.core.BaseController;
import shop.services.manage.news.NewsService;
import shop.services.manage.news.bean.News;


/**
 * 文章管理
 *
 * @author huangf
 * @author jeeshopfans
 */
@Controller
@RequestMapping("/manage/news/")
public class NewsAction extends BaseController<News> {
    private static final String page_toList = "/manage/news/newsList";
    private static final String page_toEdit = "/manage/news/newsEdit";
    private static final String page_toAdd = "/manage/news/newsAdd";

    private NewsAction() {
        super.page_toList = page_toList;
        super.page_toAdd = page_toAdd;
        super.page_toEdit = page_toEdit;
    }

    @Autowired
    private NewsService newsService;

    @Autowired
    public NewsService getService() {
        return newsService;
    }

}
