/**
 * 2012-7-8
 * jqsl2012@163.com
 */
package shop.controller.manage.indexImg;

import shop.core.Services;
import shop.services.manage.indexImg.IndexImgService;
import shop.services.manage.indexImg.bean.IndexImg;
import shop.core.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 滚动图片
 * 
 * @author huangf
 * @author jeeshopfans
 * 
 */
@Controller
@RequestMapping("/manage/indexImg/")
public class IndexImgAction extends BaseController<IndexImg> {
	private static final String page_toList = "/manage/indexImg/indexImgList";
	private IndexImgAction() {
		super.page_toList = page_toList;
	}
	@Autowired
	private IndexImgService imgService;

	@Override
	public Services<IndexImg> getService() {
		return imgService;
	}


}
