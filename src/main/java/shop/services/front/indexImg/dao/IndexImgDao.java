/**
 * 2012-7-8
 * jqsl2012@163.com
 */
package shop.services.front.indexImg.dao;

import java.util.List;

import shop.core.DaoManager;
import shop.services.front.indexImg.bean.IndexImg;


public interface IndexImgDao extends DaoManager<IndexImg> {

	/**
	 * @param i
	 * @return
	 */
	List<IndexImg> getImgsShowToIndex(int i);

}
