/**
 * 2012-7-8
 * jqsl2012@163.com
 */
package shop.services.front.news.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.front.news.bean.News;
import shop.services.front.news.dao.NewsDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;


/**
 * @author huangf
 */
@Repository("newsDaoFront")
public class NewsDaoImpl implements NewsDao {
    @Resource
	private BaseDao dao;

	public void setDao(BaseDao dao) {
		this.dao = dao;
	}

	public PagerModel selectPageList(News e) {
		return dao.selectPageList("front.news.selectPageList",
				"front.news.selectPageCount", e);
	}

	public List selectList(News e) {
		return dao.selectList("front.news.selectList", e);
	}

	public News selectOne(News e) {
		return (News) dao.selectOne("front.news.selectOne", e);
	}

	public int delete(News e) {
		return 0;
	}

	public int update(News e) {
		return  0;
	}

	/**
	 * 批量删除
	 *
	 * @param ids
	 */
	public int deletes(String[] ids) {
		return 0;
	}

	public int insert(News e) {
		return 0;
	}



	@Override
	public int deleteById(int id) {
		return 0;
	}



	
	public News selectById(String id) {
		return (News) dao.selectOne("front.news.selectById", id);
	}


}
