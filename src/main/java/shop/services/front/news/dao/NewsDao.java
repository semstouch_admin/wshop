package shop.services.front.news.dao;

import shop.core.DaoManager;
import shop.services.front.news.bean.News;


/**
 * @author huangf
 * @param
 */
public interface NewsDao extends DaoManager<News> {

}
