/**
 * 2012-7-8
 * jqsl2012@163.com
 */
package shop.services.front.news.impl;

import org.springframework.stereotype.Service;
import shop.core.ServicesManager;
import shop.services.front.news.NewsService;
import shop.services.front.news.bean.News;
import shop.services.front.news.dao.NewsDao;

import javax.annotation.Resource;


/**
 * @author huangf
 */
@Service("newsServiceFront")
public class NewsServiceImpl extends ServicesManager<News, NewsDao> implements NewsService {
    @Resource(name = "newsDaoFront")
    @Override
    public void setDao(NewsDao newsDao) {
        this.dao = newsDao;
    }

}
