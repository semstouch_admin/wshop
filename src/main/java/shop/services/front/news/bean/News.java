package shop.services.front.news.bean;

import java.io.Serializable;


public class News extends shop.services.common.News implements Serializable {

	private String catalogID;

	@Override
	public void clear() {
		super.clear();
	}

	public void setCatalogID(String catalogID) {
		this.catalogID = catalogID;
	}
}
