package shop.services.manage.news.bean;

import java.io.Serializable;


public class News extends shop.services.common.News implements Serializable {
	/**
	 * 查询条件
	 */
	private String createtimeEnd;// 页面查询条件
	private int readerCount;
	private String status;//文章是否显示到门户。y:显示；n：不显示；默认是n
	
	@Override
	public void clear() {
		super.clear();

		status = null;
		readerCount = 0;

		createtimeEnd = null;
	}

	public String getCreatetimeEnd() {
		return createtimeEnd;
	}

	public void setCreatetimeEnd(String createtimeEnd) {
		this.createtimeEnd = createtimeEnd;
	}

	public int getReaderCount() {
		return readerCount;
	}

	public void setReaderCount(int readerCount) {
		this.readerCount = readerCount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}




	@Override
	public String toString() {
		return "News [createtimeEnd=" + createtimeEnd + ", readerCount="
				+ readerCount + ", status=" + status + "]";
	}
	
	
}
