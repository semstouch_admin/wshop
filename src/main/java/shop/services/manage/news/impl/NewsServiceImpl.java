/**
 * 2012-7-8
 * jqsl2012@163.com
 */
package shop.services.manage.news.impl;

import java.util.List;

import shop.core.ServicesManager;
import shop.services.manage.news.NewsService;
import shop.services.manage.news.bean.News;
import shop.services.manage.news.dao.NewsDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * @author huangf
 */
@Service("newsServiceManage")
public class NewsServiceImpl extends ServicesManager<News, NewsDao> implements
		NewsService {
    @Resource(name = "newsDaoManage")
    @Override
    public void setDao(NewsDao newsDao) {
        this.dao = newsDao;
    }

}
