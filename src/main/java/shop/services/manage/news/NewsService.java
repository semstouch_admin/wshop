package shop.services.manage.news;

import java.util.List;

import shop.core.Services;
import shop.services.manage.news.bean.News;


public interface NewsService extends Services<News> {

}
