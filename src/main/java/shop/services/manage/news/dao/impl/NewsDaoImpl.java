/**
 * 2012-7-8
 * jqsl2012@163.com
 */
package shop.services.manage.news.dao.impl;

import java.util.List;

import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.news.bean.News;
import shop.services.manage.news.dao.NewsDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;


/**
 * @author huangf
 */
@Repository("newsDaoManage")
public class NewsDaoImpl implements NewsDao {
    @Resource
    private BaseDao dao;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(News e) {
        return dao.selectPageList("manage.news.selectPageList",
                "manage.news.selectPageCount", e);
    }

    public List selectList(News e) {
        return dao.selectList("manage.news.selectList", e);
    }

    public News selectOne(News e) {
        return (News) dao.selectOne("manage.news.selectOne", e);
    }

    public int delete(News e) {
        return dao.delete("manage.news.delete", e);
    }

    public int update(News e) {
        return dao.update("manage.news.update", e);
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public int deletes(String[] ids) {
        News e = new News();
        for (int i = 0; i < ids.length; i++) {
            e.setId(ids[i]);
            delete(e);
        }
        return 0;
    }

    public int insert(News e) {
        return dao.insert("manage.news.insert", e);
    }


    @Override
    public int deleteById(int id) {
        return dao.delete("manage.news.deleteById", id);
    }


    public News selectById(String id) {
        return (News) dao.selectOne("manage.news.selectById", id);
    }

}
