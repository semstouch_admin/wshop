/**
 * 2012-7-8
 * jqsl2012@163.com
 */
package shop.services.manage.news.dao;

import shop.core.DaoManager;
import shop.services.manage.news.bean.News;


/**
 * @author huangf
 * @param
 */
public interface NewsDao extends DaoManager<News> {


}
