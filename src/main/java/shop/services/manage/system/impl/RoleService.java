package shop.services.manage.system.impl;

import org.springframework.stereotype.Service;
import shop.core.Services;
import shop.core.common.dao.BaseDao;
import shop.core.common.dao.page.PagerModel;
import shop.services.manage.system.bean.Privilege;
import shop.services.manage.system.bean.Role;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author huangf 角色业务逻辑实现类
 */
@Service
public class RoleService implements Services<Role> {
    @Resource
    private BaseDao dao;
    @Resource
    private PrivilegeService privilegeService;

    public void setDao(BaseDao dao) {
        this.dao = dao;
    }

    public PagerModel selectPageList(Role role) {
        return dao.selectPageList("role.selectPageList",
                "role.selectPageCount", role);
    }

    public void setPrivilegeService(PrivilegeService privilegeService) {
        this.privilegeService = privilegeService;
    }

    public List selectList(Role role) {
        return dao.selectList("role.selectList", role);
    }

    public Role selectOne(Role role) {
        return (Role) dao.selectOne("role.selectOne", role);
    }

    public int insert(Role role) {
        int insertRoleId = 0;
        Privilege privilege = new Privilege();
        insertRoleId = dao.insert("role.insert",role);
        if (role.getPrivileges() == null || role.getPrivileges().trim().equals(""))
            return insertRoleId;
        String[] pArr = role.getPrivileges().split(",");
        for (int i = 0; i < pArr.length; i++) {
            privilege.clear();
            privilege.setMid(pArr[i]);
            privilege.setRid(String.valueOf(insertRoleId));
            privilegeService.insert(privilege);
        }

        return insertRoleId;
    }

    /**
     * 删除指定角色以及该角色下的所有权限
     *
     * @param role
     */
    public int delete(Role role) {
        // 删除角色
        dao.delete("role.delete", role);
        // 删除角色对应的权限
        privilegeService.deleteByRole(role);
        return 0;
    }

    public int update(Role role) {
        int updateRoleId = 0;
        Privilege privilege = new Privilege();

        updateRoleId = dao.update("role.update", role);
        privilege.setRid(String.valueOf(updateRoleId));
        privilegeService.delete(privilege);

        // 赋予权限
        if (role.getPrivileges() == null || role.getPrivileges().trim().equals(""))
            return updateRoleId;

        String[] pArr = role.getPrivileges().split(",");
        for (int i = 0; i < pArr.length; i++) {
            privilege.clear();

            privilege.setMid(pArr[i]);
            privilege.setRid(String.valueOf(updateRoleId));
            privilegeService.insert(privilege);
        }

        return updateRoleId;
    }

    /**
     * 批量删除角色
     *
     * @param ids
     */
    public int deletes(String[] ids) {
        Role role = new Role();
        for (int i = 0; i < ids.length; i++) {
            role.setId(ids[i]);
            delete(role);
            role.clear();
        }
        return 0;
    }

    @Override
    public Role selectById(String id) {
        // TODO Auto-generated method stub
        return(Role) dao.selectOne("role.selectById",id);
    }

    public Role selectByRoleName(String  roleName){
        return (Role) dao.selectOne("role.selectByRoleName",roleName);
    }
}
