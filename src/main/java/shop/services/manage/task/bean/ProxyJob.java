package shop.services.manage.task.bean;



import java.lang.reflect.Method;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class ProxyJob implements Job{
	public static final String DATA_TARGET_KEY = "target";
	public static final String DATA_TRIGGER_KEY = "trigger";
	public static final String DATA_TRIGGER_PARAM_KEY = "trigger_param";
	public static final String DATA_TASK_KEY = "task";
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap data= context.getTrigger().getJobDataMap();
		
		Object target = data.get(ProxyJob.DATA_TARGET_KEY);
		Method m = (Method)data.get(ProxyJob.DATA_TRIGGER_KEY);
		Object[] params = (Object[])data.get(ProxyJob.DATA_TRIGGER_PARAM_KEY);
		Task task = (Task)data.get(ProxyJob.DATA_TASK_KEY);
		try {
			m.invoke(target,params);
		} catch (Exception e) {			
			throw new JobExecutionException(e.getMessage(),e.getCause());
		}
		
		
	}

	
}
