package shop.services.common;

import shop.core.common.dao.QueryModel;


/**
 * 任务Bean
 * @author 草原狼
 * @date 2017-3-3
 */
public class Task extends QueryModel {
	    //任务ID，默认系统时间戳
		private String id;
		//父级任务ID
		private String parentId;
		//任务名称
		private String name;
		//任务描述
		private String desc;
		//计划执行次数,默认0,表示满足条件循环执行次数
		private int planExe = 0;
		//任务组名称,约定为一个类全名
		private String group = "";
		//任务组描述
		private String groupDesc = "";
		//任务表达式
		private String cron = "";
		//任务表达式
		private String cronDesc = "";
		//触发器，约定为一个方法名，格式com.dongnao.Xdd.methdo
		private String trigger = "";
		//触发器组名
		private String triggerGrop = "";
		//触发器组描述
		private String triggerGropDesc = "";
		//任务被执行过多少次
		private Integer execute = 0;
		//最后一次开始执行时间
		private Integer lastExeTime =0 ;
		//最后一次执行完成时间
		private Integer lastFinishTime = 0;
		//任务状态0禁用，1启动，2删除
		private Integer state = 1;
		//延迟启动，默认0,表示不延迟启动
		private Integer deply = 0;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getPlanExe() {
        return planExe;
    }

    public void setPlanExe(int planExe) {
        this.planExe = planExe;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroupDesc() {
        return groupDesc;
    }

    public void setGroupDesc(String groupDesc) {
        this.groupDesc = groupDesc;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public String getCronDesc() {
        return cronDesc;
    }

    public void setCronDesc(String cronDesc) {
        this.cronDesc = cronDesc;
    }

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }

    public String getTriggerGrop() {
        return triggerGrop;
    }

    public void setTriggerGrop(String triggerGrop) {
        this.triggerGrop = triggerGrop;
    }

    public String getTriggerGropDesc() {
        return triggerGropDesc;
    }

    public void setTriggerGropDesc(String triggerGropDesc) {
        this.triggerGropDesc = triggerGropDesc;
    }

    public int getExecute() {
        return execute;
    }

    public void setExecute(int execute) {
        this.execute = execute;
    }

    public Integer getLastExeTime() {
        return lastExeTime;
    }

    public void setLastExeTime(Integer lastExeTime) {
        this.lastExeTime = lastExeTime;
    }

    public Integer getLastFinishTime() {
        return lastFinishTime;
    }

    public void setLastFinishTime(Integer lastFinishTime) {
        this.lastFinishTime = lastFinishTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getDeply() {
        return deply;
    }

    public void setDeply(Integer deply) {
        this.deply = deply;
    }
}
