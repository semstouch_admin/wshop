<#macro htmlBase title=""  checkLogin=true>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="${systemSetting().description}"/>
    <meta name="keywords" content="${systemSetting().keywords}"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>${(title?? && title!="")?string("${systemSetting().systemCode} - "+ title , "${systemSetting().systemCode}")}</title>
    <link rel="shortcut icon" type="image/x-icon" href="${systemSetting().shortcuticon}">
    <link href="${staticpath}/bui/css/bs3/dpl.css" rel="stylesheet">
    <link href="${staticpath}/bui/css/bs3/bui.css" rel="stylesheet">
    <link rel="stylesheet" href="${staticpath}/bui-admin/index.css">
    <link rel="stylesheet" href="${staticpath}/bui/css/base.css">
    <link rel="stylesheet" href="${staticpath}/ueditor/third-party/webuploader/webuploader.css">


   <script src="${staticpath}/js/jquery-1.8.1.min.js"></script>
   <script src="${staticpath}/bui/bui.js"></script>
    <script type="text/javascript" src="${staticpath}/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" src="${staticpath}/ueditor/ueditor.all.min.js"></script>
    <script type="text/javascript" src="${staticpath}/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script src="${staticpath}/ueditor/third-party/webuploader/webuploader.js"></script>

</head>
    <#nested />
</html>
</#macro>
