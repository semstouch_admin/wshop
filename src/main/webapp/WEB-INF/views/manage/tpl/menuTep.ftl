<#macro menu menus=[]  currentMenu="首页" >
<!-- /.navbar-top-links -->
<ul  id="side-menu">
</ul>
<!-- /.sidebar-collapse -->
<!-- /.navbar-static-side -->
<script type="text/javascript">
    var Menu = BUI.Menu;
    var data =[
        <#list menus as menu>
            {
                text: '${menu.name}',
                <#assign collapsed="true">
                <#if menu.children?? && menu.children?size gt 0>
                    items: [
                        <#list menu.children as menu>
                            {
                                text: '${menu.name}',
                                href: '${rootPath}${menu.url}',
                                <#if menu.name==currentMenu>
                                    selected:true
                                    <#assign collapsed="false">
                                </#if>
                            },
                        </#list>
                    ],
                    collapsed:${collapsed},
                <#else>
                    collapsed:${collapsed},
                    href:'${menu.url}'
                </#if>
            },
        </#list>
    ];
    var sideMenu = new Menu.SideMenu({
        render:'#side-menu',
        width:200,
        prefixCls:'xm-',
        itemTpl:'<div class="xm-menu-title" data="{href}" >{text}<i class="xm-iconfont">&#xe604;</i></div>',
        collapsedCls:'xm-menu-title',
        subMenuItemTpl : '<a href="{href}">{text}</a>',
        items :data
    });

    sideMenu.render();
    sideMenu.on('itemclick', function(e){
        var menuHref =e.item.get('href');
        if(typeof(menuHref)!="undefined") {
            window.location.href = menuHref;
        }
    });
</script>

</#macro>