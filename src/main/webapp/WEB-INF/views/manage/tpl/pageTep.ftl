<#import "htmlTep.ftl" as html />
<#import "menuTep.ftl" as menu />
<#macro pageBase currentMenu>
    <@html.htmlBase >
    <body>
    <!-- Navigation -->
    <div class="header">
        <div class="dl-title">
            <span class="lp-title-port">爱优享</span><span class="dl-title-text">运营平台</span>
        </div>
        <div class="dl-log">欢迎您，<span class="dl-log-user"> ${currentUser().nickname!currentUser().username}</span><a href="${basepath}/rest/manage/user/logout" title="退出系统" class="dl-log-quit">[退出]</a>
        </div>
    </div>
    <div class="content">
        <div class="dl-tab-item">
            <div class="dl-second-nav">
                <@menu.menu menus=userMenus currentMenu=currentMenu/>
            </div>
            <div class="dl-inner-tab">
                <ol class="breadcrumb">
                    <li class="active"> ${currentMenu}</li>
                </ol>
                <div class="row" style="margin: 0 10px">
                    <#nested />
                    <!-- /.row -->
                </div>

            </div>
        </div>
    </div>
    </body>
    </@html.htmlBase>
</#macro>