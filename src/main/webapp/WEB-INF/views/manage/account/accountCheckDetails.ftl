<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="志愿者审核">
<style>
    /***循环的表单设置***/
    .bui-select .bui-select-input {
        width: 250px;
    }
</style>
<div class="xm-offline">
    <div class="row">
        <div class="panel">
            <div class="returnBtn panel-header">
                <a href="${basepath}/rest/manage/account/toCheckList">返回上一级</a>
            </div>
            <div class="panel-body">
                <form id="detailForm" class="form-horizontal" action="${basepath}/rest/manage/offline/updateJson"
                      method="get">
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            志愿者姓名：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="name"
                                   value="${e.name!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            志愿者性别：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="sex"
                                   value="${e.sex!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            志愿者手机号：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="phone"
                                   value="${e.phone!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            志愿者编号：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="id"
                                   value="${e.id!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            所在地区：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span2" name="province"
                                   value="${e.province!}" disabled="disabled"/>&nbsp;&nbsp;
                            <input type="text" class="control-text span-width span2" name="city" value="${e.city!}"
                                   disabled="disabled"/>&nbsp;&nbsp;
                            <input type="text" class="control-text span-width span2" name="area" value="${e.area!}"
                                   disabled="disabled"/>&nbsp;&nbsp;
                            <input type="text" class="control-text span-width span10" name="address"
                                   value="${e.address!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            最高学历：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text" name="education" value="" disabled="disabled"
                                   id="education"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            政治面貌：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text" name="politics" value="" disabled="disabled"
                                   id="face"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            意向服务领域：
                        </label>

                        <div class="controls" >
                            <input  class="control-text span-width  span6" type="text"  name="field" value="${e.field!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            意向服务地区：
                        </label>

                        <div class="controls bui-form-group-select" data-type="city">
                            <input type="text" class="control-text span-width span2" name="serviceProvince"
                                   value="${e.serviceProvince!}"
                                   disabled="disabled"/>&nbsp;&nbsp;
                            <input type="text" class="control-text span-width span2" name="serviceCity" value="${e.serviceCity!}"
                                   disabled="disabled"/>&nbsp;&nbsp;
                            <input type="text" class="control-text span-width span2" name="serviceArea" value="${e.serviceArea!}"
                                   disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            头像照片：
                        </label>
                        <img src="${e.headimgurl!}"
                             style="height: 100px;width: 100px;margin:0 0 10px 10px " name="headimgurl">
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            身份证号码：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="idCard"
                                   value="${e.idCard!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            QQ：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="qq"
                                   value="${e.qq!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            微信：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="weChar"
                                   value="${e.weChar!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            审批结果：</label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="status" value=""
                                   disabled="disabled" id="status"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var Grid = BUI.Grid;
    var Store = BUI.Data.Store;
    var Overlay = BUI.Overlay;
    var editForm = new BUI.Form.Form({
        srcNode: '#detailForm',
        submitType: 'ajax',
        callback: function (data) {
        }
    }).render();
    //对审核结果的状态做判断塞值到input中
    var status = "${e.status!}";
    if (status == "0") {
        $("#status").val("待审核");
    }if (status == "1") {
        $("#status").val("已通过");
    }if(status == "2"){
        $("#status").val("不通过");
    }

    //对最高学历做判断塞值到input中
    var education = "${e.education!}";
    if (education == "1") {
        $("#education").val("小学");
    }
    if (education == "2") {
        $("#education").val("初中");
    }
    if (education == "2") {
        $("#education").val("高中");
    }
    if (education == "3") {
        $("#education").val("大专");
    }
    if (education == "4") {
        $("#education").val("本科");
    }
    if (education == "5") {
        $("#education").val("研究生");
    }
    if (education == "6") {
        $("#education").val("博士");
    } else {
        $("#education").val("博士以上");
    }
    //对政治面貌做判断塞值到input中
    var face = "${e.politics!}";
    if (face == "1") {
        $("#face").val("团员");
    }
    if (face == "2") {
        $("#face").val("预备党员");
    }
    if (face == "3") {
        $("#face").val("党员");
    }
    if (face == "4") {
        $("#face").val("党员（保留团籍）");
    } else {
        $("#face").val("其他");
    }

</script>

</@page.pageBase>