<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="志愿者审核">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }
    .button-primary {
        margin-left: 5px;
    }
</style>
<form id="searchForm" class="form-panel" action="${basepath}/rest/manage/account/loadData">
    <ul class="panel-content">
        <li>
            <div class="control-group span5">
                <div class="search-controls  controls">
                    <input type="text" name="name" id="title" value="" placeholder="搜索姓名、手机号码">
                </div>
            </div>
            <div class="form-actions span2">
                <button type="submit" class="button  button-primary">
                    搜索
                </button>
            </div>
        </li>
    </ul>
</form>

<div id="grid"></div>
<script>
    /*-------------------------------------------------------- 表格数据渲染start-----------------------------------*/
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '序号', dataIndex: 'id', elCls: 'center', width: '10%'},
                {title: '志愿者姓名', dataIndex: 'name', elCls: 'center', width: '15%'},
                {title: '志愿者性别', dataIndex: 'sex', elCls: 'center', width: '15%'},
                {title: '志愿者手机号', dataIndex: 'phone', elCls: 'center', width: '15%'},
                {title: '身份证号码', dataIndex: 'idCard', elCls: 'center', width: '15%'},
                {
                    title: '审批状态', dataIndex: 'status', elCls: 'center', width: '15%', renderer: function (data) {
                    if (data == "0") {
                        return '待审核';
                    }
                    if (data == "1") {
                        return '已通过';
                    }
                    if (data == "2") {
                        return '不通过';
                    }
                    if (data == "3") {
                        return '未认证';
                    }
                }
                },
                {
                    title: '操作', dataIndex: 'id', elCls: 'center', width: '15%', renderer: function (value, data) {
                    if (data.status == "0") {
                        return '<a href="${basepath}/rest/manage/account/toCheckEdit?id=' + value + '">审批</a>';
                    }
                    else {
                        return '<a href="${basepath}/rest/manage/account/toCheckDetails?id=' + value + '">查看</a>';
                    }

                }
                }
            ];
    var store = new Store({
                url: 'loadData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10'
                },
                pageSize: 10,	// 配置分页数目
                root: 'list'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格

                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();

    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });
    /*-------------------------------------------------------- 表格数据渲染end-----------------------------------*/

</script>
</@page.pageBase>