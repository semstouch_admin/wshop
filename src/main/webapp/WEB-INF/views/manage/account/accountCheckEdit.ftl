<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="志愿者审核">
<style>
    /***循环的表单设置***/
    .bui-select .bui-select-input {
        width: 250px;
    }
    .button-large {
          padding: 10px 30px;
          font-size: 16px;
      }
</style>
<div class="xm-offline">
    <div class="row">
        <div class="panel">
            <div class="returnBtn panel-header">
                <a href="${basepath}/rest/manage/account/toCheckList">返回上一级</a>
            </div>
            <div class="panel-body">
                <form id="detailForm" class="form-horizontal" action="${basepath}/rest/manage/account/updateJson">
                    <input type="hidden" class="control-text span-width span10" name="id" value="${account.id!}">

                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            志愿者姓名：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="name"
                                   disabled="disabled" value="${account.name!}"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            志愿者性别：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="sex"
                                   disabled="disabled" value="${account.sex!}"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            志愿者手机号：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="phone"
                                   disabled="disabled" value="${account.phone!}"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            志愿者编号：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="id"
                                   disabled="disabled" value="${account.id!}"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            所在地区：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span2" name="province"
                                   value="${account.province!}" disabled="disabled"/>&nbsp;&nbsp;
                            <input type="text" class="control-text span-width span2" name="city"
                                   value="${account.city!}"
                                   disabled="disabled"/>&nbsp;&nbsp;
                            <input type="text" class="control-text span-width span2" name="area"
                                   value="${account.area!}"
                                   disabled="disabled"/>&nbsp;&nbsp;
                            <input type="text" class="control-text span-width span10" name="address"
                                   value="${account.address!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            最高学历：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text" name="education" value="" disabled="disabled"
                                   id="education"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            政治面貌：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text" name="politics" value="" disabled="disabled"
                                   id="face"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            意向服务领域：
                        </label>

                        <div class="controls">
                            <input class="control-text span-width  span6" type="text" name="field" value="${account.field!}"
                                   disabled="disabled"/>
                        </div>

                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            意向服务地区：
                        </label>

                        <div class="controls bui-form-group-select" data-type="city">
                            <input type="text" class="control-text span-width span2" name="serviceProvince"
                                   value="${account.serviceProvince!}"
                                   disabled="disabled"/>&nbsp;&nbsp;
                            <input type="text" class="control-text span-width span2" name="serviceCity"
                                   value="${account.serviceCity!}"
                                   disabled="disabled"/>&nbsp;&nbsp;
                            <input type="text" class="control-text span-width span2" name="serviceArea"
                                   value="${account.serviceArea!}"
                                   disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            头像照片：
                        </label>
                        <img src="${account.headimgurl!}"
                             style="height: 100px;width: 100px;margin:0 0 10px 10px " >
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            身份证号码：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="idCard"
                                   value="${account.idCard!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            QQ：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="qq"
                                   value="${account.qq!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            微信：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span6" name="weChar"
                                   value="${account.weChar!}" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="centered">
                        <a class="button  button-large button-success" id="btnShow">审核</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<#-------------------------------------------------------编辑审核状态 begin-------------------------------------------------------->
<div id="editContent" style="display:none;">
    <form id="editForm" class="form-horizontal" action="${basepath}/rest/manage/account/updateAccountJson" method="post">
        <input type="hidden" class="input-normal control-text" name="id" value="${account.id!}"/>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">审核状态：</label>

                <div class="controls control-row4">
                    <select data-rules="{required:true}" name="status" value="${account.status!}" id="status">
                        <option value="">-请选择-</option>
                        <option value="1">通过</option>
                        <option value="0">未审核</option>
                        <option value="2">不通过</option>
                    </select>
                </div>
            </div>
        </div>
    </form>
<#-------------------------------------------------------审核状态 end-------------------------------------------------------->
</div>
<script>
    var Grid = BUI.Grid;
    var Store = BUI.Data.Store;
    var Overlay = BUI.Overlay;
    var editForm = new BUI.Form.Form({
        srcNode: '#editForm',
        submitType: 'ajax',
        callback: function (data) {
            if (data.status) {
                window.location.href = "${basepath}/rest/manage/account/toList"
            } else {
                window.location.href = "${basepath}/rest/manage/account/toCheckList"
            }
        }
    });
    editForm.render();
    var editDialog = new BUI.Overlay.Dialog({
        title: '编辑审核状态',
        width: 500,
        height: 120,
        contentId: 'editContent',
        success: function () {
            editForm.ajaxSubmit();
        }
    });
    //编辑按钮事件
    $('#btnShow').on('click', function () {
        editDialog.show();
    });
    //对最高学历做判断塞值到input中
    var education = "${account.education!}";
    if (education == "1") {
        $("#education").val("小学");
    }
    if (education == "2") {
        $("#education").val("初中");
    }
    if (education == "2") {
        $("#education").val("高中");
    }
    if (education == "3") {
        $("#education").val("大专");
    }
    if (education == "4") {
        $("#education").val("本科");
    }
    if (education == "5") {
        $("#education").val("研究生");
    }
    if (education == "6") {
        $("#education").val("博士");
    } else {
        $("#education").val("博士以上");
    }
    //对政治面貌做判断塞值到input中
    var face = "${account.politics!}";
    if (face == "1") {
        $("#face").val("团员");
    }
    if (face == "2") {
        $("#face").val("预备党员");
    }
    if (face == "3") {
        $("#face").val("党员");
    }
    if (face == "4") {
        $("#face").val("党员（保留团籍）");
    } else {
        $("#face").val("其他");
    }
    /*--------------------------对服务领域做判断塞值到input中  begin--------------------*/

    /*--------------------------对服务领域做判断塞值到input中  end--------------------*/
</script>



</@page.pageBase>