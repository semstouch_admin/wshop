<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="首页图片">
<style>
    .button-success, .button-danger {
        margin-left: 5px;
    }

    .bui-stdmod-body {
        height: 320px !important;
    }
    .row p{text-align:center;font-size: 12px;color:#999;-webkit-transform-origin-x:0;-webkit-transform: scale(0.85);}
    .form-horizontal .control-group-img{height:40px;}
</style>
<form id="searchForm" class="form-panel">
    <ul class="panel-content">
        <li>
            <div class="form-actions">
            <#--<a href="javascript:;" class="button button-primary">-->
            <#--<i class="icon-plus-sign icon-white"></i> 查询-->
            <#--</a>-->
                <a href="javascript:add()" class="button button-success">添加
                </a>
                <a class="button button-danger" onclick="return delFunction();">删除
                </a>
            </div>
        </li>
    </ul>
</form>
<#----------------------------------------------搜索表单 end---------------------------------------------------------->
<#----------------------------------------------新增表单start--------------------------------------------------------->
<div id="addContent" style="display:none;">
    <form id="addForm" class="form-horizontal" action="${basepath}/rest/manage/indexImg/insertJson" method="post">
        <input type="hidden" class="input-normal control-text" name="id"/>

        <div class="row">
            <div class="control-group span16">
                <label class="control-label">标题：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="title" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">类型：</label>

                <div class="controls">
                    <select data-rules="{required:true}" name="type">
                        <option value="">-请选择-</option>
                        <option value="n">首页轮播图</option>
                        <option value="s">商城首页轮播图</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">图片地址：</label>

                <div class="control-group-img  controls ">
                    <input type="text" class="input-normal control-text" name="picture" data-rules="{required : true}"
                           readonly>
                    <a id="addImgBtn" class="button button-min button-primary pull-right"
                       style="height:20px;margin-left: 10px;">上传</a>
                </div>

            </div>
            <p>轮播图最佳效果分辨率为1080*558</p>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">广告链接：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="link" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">排序：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="order1" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">描述：</label>

                <div class="controls control-row4">
                    <input type="text" class="input-normal control-text" name="desc1" data-rules="{required : true}">
                </div>
            </div>
        </div>
    </form>
</div>
<#----------------------------------------------新增表单 end---------------------------------------------------------->
<#----------------------------------------------编辑表单start--------------------------------------------------------->
<div id="editContent" style="display:none;">
    <form id="editForm" class="form-horizontal" action="${basepath}/rest/manage/indexImg/updateJson" method="post">
        <input type="hidden" class="input-normal control-text" name="id"/>

        <div class="row">
            <div class="control-group span16">
                <label class="control-label">标题：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="title" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">类型：</label>

                <div class="controls">
                    <select data-rules="{required:true}" name="type">
                        <option value="">-请选择-</option>
                        <option value="n">首页轮播图</option>
                        <option value="s">商城首页轮播图</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">图片地址：</label>

                <div class="control-group-img controls">
                    <input type="text" class="input-normal control-text" name="picture" data-rules="{required : true}"
                           readonly>
                    <a id="editImgBtn" class="button button-min button-primary pull-right"
                       style="height:20px;margin-left: 10px;">上传</a>
                </div>
            </div>
            <p>轮播图最佳效果分辨率为1080*558</p>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">广告链接：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="link" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">排序：</label>

                <div class="controls">
                    <input type="text" class="input-normal control-text" name="order1" data-rules="{required : true}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group span16">
                <label class="control-label">描述：</label>

                <div class="controls control-row4">
                    <input type="text" class="input-normal control-text" name="desc1" data-rules="{required : true}">
                </div>
            </div>
        </div>
    </form>
</div>
<#----------------------------------------------编辑表单 end---------------------------------------------------------->

</div>
<div id="grid"></div>
<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '标题', dataIndex: 'title', width: '20%', elCls: 'center'},
                {
                    title: '图片', dataIndex: 'picture', width: '20%', elCls: 'center', renderer: function (value) {
                    return '<img src="${basepath}/' + value + '" width="100px" height="100px"/>'
                }
                },{
                    title: '类型', dataIndex: 'type', elCls: 'center', width: '20%', renderer: function (data) {
                        if (data == "n") {
                            return '首页轮播图';
                        }
                        if (data == "s") {
                            return '商城首页轮播图';
                        }
                    }
                },
                {title: '排序', dataIndex: 'order1', width: '20%', elCls: 'center'},
                {
                    title: '操作', dataIndex: 'id', width: '20%', elCls: 'center', renderer: function (value) {

                    <#if checkPrivilege("/manage/indexImg/edit")>
                        return '<a href="javascript:edit(' + value + ')">编辑</a>';
                    <#else>
                        return "";
                    </#if>


                }
                }
            ];
    var store = new Store({
                url: 'loadData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    status: $("#status").val()
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格

                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();

    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });
    /*------------------------------------------------新增表单处理start------------------------------------------------*/
    var addForm = new BUI.Form.Form({
        srcNode: '#addForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
            addDialog.close();
        }
    }).render();

    var addDialog = new BUI.Overlay.Dialog({
        title: '新增图片',
        width: 500,
        height: 320,
        contentId: 'addContent',
        success: function () {
            addForm.ajaxSubmit();
        }
    });
    //添加按钮事件
    function add() {
        addDialog.show();
        var addUploader = WebUploader.create({
            auto: true,
            swf: '${staticpath}/ueditor/third-party/webuploader/Uploader.swf',
            server: '${basepath}/rest/manage/ued/config?action=uploadimage',
            pick: '#addImgBtn',
            resize: false,
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/gif,image/jpg,image/jpeg,image/bmp,image/png,'
            }
        });

        addUploader.on('uploadSuccess', function (file, response) {
            addUploader.removeFile(file);
            addForm.setFieldValue("picture", response.url)
        });
    }
    /*------------------------------------------------新增表单处理 end-------------------------------------------------*/
    /*------------------------------------------------编辑表单处理 start-----------------------------------------------*/
    var editForm = new BUI.Form.Form({
        srcNode: '#editForm',
        submitType: 'ajax',
        callback: function (data) {
            var obj = new Object();
            obj.start = 0; //返回第一页
            store.load(obj);
            editDialog.close();
        }
    });
    editForm.render();

    var editDialog = new BUI.Overlay.Dialog({
        title: '编辑图片',
        width: 500,
        height: 320,
        contentId: 'editContent',
        success: function () {
            editForm.ajaxSubmit();
        }
    });
    //编辑按钮事件
    function edit(id) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "${basepath}/rest/manage/indexImg/toEditJson",
            data: {id: id},
            success: function (data) {
                var form = $("#editForm")[0];
                BUI.FormHelper.setFields(form, data.data);
                editDialog.show();
                var editUploader = WebUploader.create({
                    auto: true,
                    swf: '${staticpath}/ueditor/third-party/webuploader/Uploader.swf',
                    server: '${basepath}/rest/manage/ued/config?action=uploadimage',
                    pick: '#editImgBtn',
                    resize: false,
                    accept: {
                        title: 'Images',
                        extensions: 'gif,jpg,jpeg,bmp,png',
                        mimeTypes: 'image/gif,image/jpg,image/jpeg,image/bmp,image/png,'
                    }
                });

                editUploader.on('uploadSuccess', function (file, response) {
                    editUploader.removeFile(file);
                    editForm.setFieldValue("picture", response.url)
                });
            }
        });
    }
    /*------------------------------------------------编辑表单处理 end------------------------------------------------*/

    //删除选中的记录
    function delFunction() {
        var selections = grid.getSelection();
        var ids = new Array();
        for (var i = 0; i < selections.length; i++) {
            ids[i] = selections[i].id.toString()
        }
        $.ajax({
            type: "POST",
            url: "${basepath}/rest/manage/indexImg/deletesJson",
            dataType: "json",
            data: {
                ids: ids
            },
            success: function (data) {
                var obj = form.serializeToObject();
                obj.start = 0; //返回第一页
                store.load(obj);
            }
        });

    }


</script>
</@page.pageBase>