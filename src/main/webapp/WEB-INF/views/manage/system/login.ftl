<#import "../tpl/htmlTep.ftl" as html>

<@html.htmlBase checkLogin=false >
<style type="text/css">
    html,body{width: 100%;height:100%;min-width: 1024px;font-family: '黑体';font-size: 16px;}
    h3{font-family: '黑体';}
    body{background: url("${staticpath}/images/loginBg.jpg") no-repeat;background-size: cover;}
    .user_register{margin-right: 10px;}
    .top-head{height: 84px;line-height: 84px;position: relative}
    .top-head:after{z-index: -1; display: block;content: '';width: 100%;height: 84px;position: absolute;background: #000;opacity: .2;top: 0;  left: 0;}
    .top-head h3{font-size: 32px;color:#fff;z-index: 1;height: 84px;line-height: 84px;padding-left: 20%;}
    .login-box{margin-right: 20%;}
    .well{background: #fff;box-shadow: none;-webkit-box-shadow:none;}
    .w310{width: 310px;margin: 0 auto!important;padding-top: 20px!important;}
    .button-large{border-radius: 2px;}
    .w310 a,.w310 button{width: 138px;  height: 45px; padding: 0;line-height: 45px;font-size: 16px;color: #fff;text-decoration: none;}
    .mb20{margin-bottom: 20px!important;}
    .error-tips{color: red;margin-left: 30px;padding-top: 10px;}
    .center{text-align: center;}
    .xm-login-panel{margin-top: 21%;}
    .login-box .panel,.login-box .panel-header{background: #FFFFFF;border-bottom: 0;}
    .login-box input{height: 45px;width: 310px;line-height: 45px;margin-left: 6px;}
    .login-box {margin-bottom: 20px;}
    .panel-header h3{text-indent: 10px;background: #fff;font-size: 23px;}
</style>
<body>
<#--header-->
<div class="top-head">
    <h3>${systemSetting().name}后台系统</h3>
</div>
<div class="row span10 pull-right login-box">
    <div class="panel xm-login-panel">
        <div class="panel-header" >
            <h3 class="panel-title" >用户登录</h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal well" action="${basepath}/rest/manage/user/login" method="post">
                <div class="row mb20 center">
                      <input type="text" value="${e.username!""}" placeholder="账号" name="username" class="control-text" id="username" autofocus/>
                </div>
                <div class="row center ">
                    <input type="text" onfocus="this.type='password'" name="password" placeholder="密码" class="control-text" label="密码"/>
                </div>
                <#if errorMsg??>
                    <div class="row">
                        <div class="error-tips">*${errorMsg}</div>
                    </div>
                </#if>
                <div class="row w310">
                    <a  href="${basepath}/rest/manage/organize/toRegisterOrganize" class="pull-left button button-large button-primary user_register">注册</a>
                    <button type="submit" class="pull-right button button-large button-primary"> <a>登陆</a></button>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
</body>
</@html.htmlBase>