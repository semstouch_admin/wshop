<#import "../tpl/htmlTep.ftl" as html>

<@html.htmlBase checkLogin=false >
<style>
    body{font-size: 16px;}
    html,body,h3{width: 100%;height:100%;position: relative;font-family: '黑体'}
    body:before{display: block;content: '';height: 10px;width: 100%;z-index: -1;top:84px;position: absolute;
        FILTER: progid:DXImageTransform.Microsoft.Gradient(gradientType=0,startColorStr=#d2d2d2,endColorStr=#ffffff); /*IE 6 7 8*/
        background: -ms-linear-gradient(top, #d2d2d2,  #ffffff);        /* IE 10 */
        background:-moz-linear-gradient(top,#d2d2d2,#ffffff);/*火狐*/
        background:-webkit-gradient(linear, 0% 0%, 0% 100%,from(#d2d2d2), to(#ffffff));/*谷歌*/
        background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#d2d2d2), to(#ffffff));      /* Safari 4-5, Chrome 1-9*/
        background: -webkit-linear-gradient(top, #d2d2d2, #ffffff);   /*Safari5.1 Chrome 10+*/
        background: -o-linear-gradient(top, #d2d2d2, #ffffff);  /*Opera 11.10+*/
    }
    input[type="text"], input[type="password"]{width: 273px;border-radius: 2px;background: #f7f7f7;}
    a:hover{text-decoration: none;}
    .wrap{width: 1024px;margin: 0 auto}
    .top-head{height: 84px;line-height: 84px;position: relative}
    .top-head h3{font-size: 32px;z-index: 1;height: 84px;line-height: 84px;}
    .xm-login-panel{margin: 0;}
    .panel,.panel-header,.panel-body,.well{background: #fff!important;border: none!important;border-color: #fff!important;;}
    .well{box-shadow: none;-webkit-box-shadow:none;}
    .register{margin-top: 20px;}
    .back-btn{color: #61b0f3;line-height: 1em;margin-top: 10px;}
    .code_btn{width: 95px;height: 40px;padding:0;border-radius: 2px;line-height: 40px;}
    #registerBtn{background: #65e391;width: 299px;height: 40px;line-height: 40px;color: #FFFFFF;border-color: transparent;padding: 0;border-radius: 2px;margin-left: 130px;}
</style>
<body>

<div class="wrap">
    <div class="top-head">
        <h3>${systemSetting().name}后台系统
            <a href="/manage/system/toLogin" class="back-btn pull-right" >
                <img src="${staticpath}/images/backIcon.png" alt=""/>
                <p style="margin:5px 0 0 0;font-size: 14px;">返回</p>
            </a>
        </h3>
    </div>
    <div class="row span16 register">
        <div class="panel xm-login-panel">
            <div class="panel-header">
                <h3 class="panel-title" style="height: 30px;line-height: 30px;"><b style="font-size: 24px;font-weight: bold;color:#777;">用户注册</b></h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal well" id="registerForm" action="${basepath}/rest/manage/organize/insertJson"
                      method="post">
                    <div class="row">
                        <div class="control-group span20">
                            <label class="control-label">
                                <s>*</s>机构名称：</label>
                            <div class="controls">
                                <input type="text"  placeholder="机构名称" name="name" class="control-text"
                                       data-rules="{required:true}" data-messages="{required:'选项不能为空'}"  autofocus/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="control-group span20">
                            <label class="control-label">
                                <s>*</s>负责人：</label>
                            <div class="controls">
                                <input type="text" name="principal" placeholder="负责人"data-rules="{required:true}" data-messages="{required:'选项不能为空'}" class="control-text" label="负责人"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="control-group span20">
                            <label class="control-label">
                                <s>*</s>手机号：</label>
                            <div class="controls">
                                <input type="tel" name="principalPhone" placeholder="请输入手机号"data-rules="{required:true}" data-messages="{required:'选项不能为空'}" class="control-text" label="手机号"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row">
                            <div class="control-group span20">
                                <label class="control-label">
                                    <s>*</s>验证码：</label>
                                <div class="controls">
                                    <input type="text" style="width: 164px;" name="SmsContent" placeholder="请输入验证码"data-rules="{required:true}" data-messages="{required:'选项不能为空'}" class="control-text" label="密码"/>
                                    <input type="hidden" id="SmsId" name="SmsId" value=""/>
                                    <a type="button" class="code_btn button  button-primary" >验证码</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="control-group span20">
                                <label class="control-label">
                                    <s>*</s> 密码：</label>
                                <div class="controls">
                                    <input type="text" onfocus="this.type='password'" name="password" placeholder="请输入密码" data-rules="{required:true}" data-messages="{required:'选项不能为空'}" class="control-text" label="密码"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="control-group span8 offset3 " style="margin-bottom: 10px;font-size: 12px;color: #999999;">
                                *注册后无法修改，请如实填写信息。
                            </div>
                        </div>
                        <div class="form-actions ">
                            <button type="button" id="registerBtn" class="button button-large button-primary">注册</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
</div>

    <script>
        var Form = BUI.Form;

        new Form.Form({
            srcNode : '#registerForm'
        }).render();


        function getCode() {
            var phone = $('input[name="principalPhone"]').val();
            if (!checkMobile(phone)) {
                BUI.Message.Alert('手机格式错误','error');
                return;
            }

            var btn = $('.code_btn');
            btn.off('click');
            btn.css({'background-color': '#CCCCCC','border-color':'#CCCCCC'});
            var showTime = function (second) {
                setTimeout(function () {
                    if (second == 0) {
                        btn.css('background-color', '');
                        btn.addClass('getCode');
                        btn.text('获取验证码');
                        $('.getCode').on('click', function () {
                            getCode();
                        });
                        return;
                    }
                    btn.text(second + ' 秒');
                    second --;
                    showTime(second);
                }, 1000);
            };
            $.ajax({
                type: 'POST',
                url: '/manage/organize/getSMSCode',
                dataType: 'json',
                data:{
                    mobile:phone
                },
                success:function(res){
                    if(res.success){
                        showTime(60);
                        BUI.Message.Alert('发送成功','success');
                        $('#SmsId').val(res.data.sessionId);
                    }else{
                        BUI.Message.Alert(res.message+',发送失败','error');
                        $('.code_btn').css('background-color','')
                    }

                }
            } )
        }

        $('.code_btn').on('click', function () {
            getCode();
        });
        function checkMobile(str) {
            var re = /^1[356789]\d{9}$/;
            if (re.test(str)) {
                return true;
            } else {
                return false;
            }
        }
        //       表单提交
        var registerForm = new BUI.Form.Form({
            srcNode: '#registerForm',
            submitType: 'ajax',
            callback: function (data) {
                BUI.Message.Alert('注册成功','success');
                setTimeout(function(){
                    window.location.href='/'
                },2000)
            }
        });
        $('#registerBtn').click(function(){
            registerForm.ajaxSubmit();
        })


    </script>
</body>

</@html.htmlBase>