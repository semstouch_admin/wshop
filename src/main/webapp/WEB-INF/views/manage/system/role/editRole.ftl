<#import "../../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="角色管理">
<div class="xm-offline">
    <div class="row">
        <div class="panel">
            <div class="panel-header">
                <a href="${basepath}/rest/manage/role/toList">返回上一级</a>
            </div>
            <div class="panel-body">
                <div id="t1" class="span6 doc-content"></div>
                <div class="span18 doc-content">
                    <form class="form-horizontal well" id="J_Form" action="${basepath}/rest/manage/role/updateJson"
                          method="post" style="height:464px;">
                        <h3 style="padding:0 10px 10px 10px;border-bottom: #c3c3d6 1px solid;">角色信息</h3>

                        <div class="row">
                            <div class="control-group span8">
                                <label class="control-label">角色名称：</label>

                                <div class="controls">
                                    <input type="hidden" class="control-text" name="id">
                                    <input type="text" class="control-text" name="role_name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="control-group span8">
                                <label class="control-label">角色描述：</label>

                                <div class="controls">
                                    <input type="text" name="role_desc" class="control-text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="control-group span8">
                                <label class="control-label">数据库权限：</label>

                                <div class="controls">
                                    <select name="role_dbPrivilege" class="input-normal">
                                        <option value="select">select</option>
                                        <option value="select,insert">select,insert</option>
                                        <option value="select,insert,update">select,insert,update</option>
                                        <option value="select,insert,update,delete">select,insert,update,delete</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="control-group span16">
                                <label class="control-label">状态：</label>

                                <div class="controls">
                                    <select name="status" class="input-normal">
                                        <option value="y">启用</option>
                                        <option value="n">禁用</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="control-group span16">
                                <label class="control-label">角色权限：</label>

                                <div class="controls">
                                    <input type="text" name="privileges" class="control-text" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-actions offset3">
                                <button type="submit" class="button button-success">立即保存</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function getQueryString(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]);
        return null;
    }

    var store = new BUI.Data.TreeStore({
        map: {
            name: 'text'
        },
        url: '${basepath}/rest/manage/menu/getMenusByPid?pid=0',
        params: {         //设置请求时的参数
            id: getQueryString("id")
        }
    });

    var form = new BUI.Form.HForm({ //创建表单
        srcNode: '#J_Form',
        submitType: 'ajax',
        callback: function (data) {
            if (data.success) {
                window.location.href = "${basepath}/rest/manage/role/toList";
            } else {

            }
        }
    }).render();

    form.on('beforesubmit', function (ev) {
        form.ajaxSubmit();
        return false;
    });

    store.on('load', function (ev) {
        var checkedNodes = store.findNodesBy(function (node) {
            if (node.checked == true) {
                return true;
            }
            return false;
        });
        var ids = new Array();
        BUI.each(checkedNodes, function (node) {
            ids.push(node.id);
        });
        form.setFieldValue("privileges", ids.join(','));
    });

    BUI.use('bui/tree', function (Tree) {
        var tree = new Tree.TreeList({
            render: '#t1',
            store: store,
            height: 500,
            showLine: true, //显示连接线
            cascadeCheckd: false, //不级联勾选
            'itemclick': true
        });
        tree.render();
        store.load();
        tree.on('checkedchange', function (ev) {
            var ids = new Array();
            var checkedNodes = tree.getCheckedNodes();
            BUI.each(checkedNodes, function (node) {
                ids.push(node.id);
            });
            form.setFieldValue("privileges", ids.join(','));
        });
    });

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "${basepath}/rest/manage/role/toEditJson",
        data: {
            id: getQueryString("id")
        },
        success: function (data) {
            var form = $("#J_Form")[0];
            BUI.FormHelper.setFields(form, data.data);
        }
    });

</script>
</@page.pageBase>