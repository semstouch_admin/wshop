<#import "../../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="角色管理">
<form id="searchForm" class="form-panel">
    <ul class="panel-content">
        <li>
        <div class="form-actions">
			<#if checkPrivilege("role/insert")>
                <a href="${basepath}/rest/manage/role/toAdd" class="button button-success">添加</a>
			</#if>
            <button  class="button button-danger"  onclick="return delFunction()">
                删除
            </button>
        </div>
        </li>
    </ul>
</form>
<div id="grid"></div>

<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title : '角色名称',dataIndex :'role_name', width:'20%',elCls: 'center'},
                {title : '角色描述',dataIndex :'role_desc',  width:'40%',elCls: 'center'},
                {title : '数据库权限',dataIndex : 'role_dbPrivilege', width:'20%',elCls: 'center'},
                {title : '状态',dataIndex : 'status',width:'10%',elCls: 'center', renderer:function(data){
                    if(data == "y"){
                        return '<img src="${basepath}/resource/images/action_check.gif">';
                    } else {
                        return '<img src="${basepath}/resource/images/action_delete.gif">';
                    }
                }},
                {title : '操作',dataIndex : 'id',width:'10%',elCls: 'center',renderer : function (value) {
                     return '<a href="${basepath}/rest/manage/role/toEdit?id=' + value + '">编辑</a>';
                }}
            ];

    var store = new Store({
                url : 'loadData',
                autoLoad:true, //自动加载数据
                params : { //配置初始请求的参数
                    length : '10',
                    status:$("#status").val()
                },
                pageSize:10,	// 配置分页数目
                root : 'list',
                totalProperty : 'total'
            }),
            grid = new Grid.Grid({
                render:'#grid',
                columns : columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins : [Grid.Plugins.CheckSelection], // 插件形式引入多选表格
                // 底部工具栏
                bbar:{
                    pagingBar:true
                }
            });

    grid.render();

    var form = new BUI.Form.HForm({
        srcNode : '#searchForm'
    }).render();

    form.on('beforesubmit',function(ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });


    //删除选中的记录
    function delFunction(){
        var selections = grid.getSelection();
        var ids=new Array();
        for(var i=0;i<selections.length;i++){
            ids[i]=selections[i].id.toString()
        }
        $.ajax({
            type: "POST",
            url: "${basepath}/rest/manage/role/deletesJson",
            dataType: "json",
            data: {
                ids:ids
            },
            success: function (data) {
                var obj = form.serializeToObject();
                obj.start = 0; //返回第一页
                store.load(obj);
            }
        });

    }

</script>


</@page.pageBase>