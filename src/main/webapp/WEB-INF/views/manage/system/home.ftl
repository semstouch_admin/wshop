<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="系统概况">
<style>
    .breadcrumb {
        margin-bottom: 10px;;
    }

    .row {
        background-color: #ffffff;
    }

    h3 {
        float: left;
        margin: 0 0 0 16px;
        pading: 0;
        font-size: 16px;
    }

    .information-ul {
        width: 100%;
        height: 134px;
        padding: 0;
        margin: 0;
    }

    .information-ul li {
        margin: 10px 1.5%;
        height: 120px;
        width: 220px;
        background-color: #ecf2f8;
        float: left;
        border-radius: 4px;
    }

    .information-ul li div {
        margin: 0;
        padding-top: 30px;
        padding-left: 15px;
        height: 44px;
    }

    .money-information div {
        margin: 15px 0 0 15px;
        height: 26px;;
    }

    .money-information img {
        float: left;
    }

    .money-information div img {
        width: 24px;
        height: 24px;
        float: left;
    }

    .money-information .information-ul img {
        width: 24px;
        height: 21px;
        margin-top: 10px;
    }

    .bean-love {
        width: 18px !important;
        height: 24px !important;
        margin-top: 8px !important;
    }

    .circular-img {
        width: 22px !important;
        height: 22px !important;

    }

    .user-img {
        width: 20px !important;
        height: 26px !important;
        margin-top: 6px !important;
    }

    .money-information div h1 {
        float: left;
        font-size: 28px;
        margin-left: 10px;
        color: #717a84;
        font-weight: bold;
    }

    h5 {
        font-size: 16px;
        margin-left: 50px;
        color: #717a84;
    }

    hr {
        margin-top: 10px;
        margin-bottom: 10px;

    }

</style>

<div class="money-information">
    <div>
        <img src="${staticpath}/after/images/crown.png" alt=""/>

        <h3>资金概况</h3>
    </div>
    <hr/>
    <ul class="information-ul">
        <li>
            <div>
                <img src="${staticpath}/after/images/money.png">

                <h1>88,150.00</h1>
            </div>
            <h5>总销售金额</h5>
        </li>
        <li>
            <div>
                <img src="${staticpath}/after/images/love1.png" class="bean-love">

                <h1>8,165</h1>
            </div>
            <h5>爱心豆总数</h5>
        </li>
        <li>
            <div>
                <img src="${staticpath}/after/images/love2.png" class="bean-love">

                <h1>8,000</h1>
            </div>
            <h5>已提现爱心豆</h5>
        </li>
        <li>
            <div>
                <img src="${staticpath}/after/images/love3.png" class="bean-love">

                <h1>8,000</h1>
            </div>
            <h5>已提现爱心豆</h5>
        </li>
    </ul>

</div>
<div class=" money-information">
    <div>
        <img src="${staticpath}/after/images/build.png" alt=""/>

        <h3>机构概况</h3>

    </div>
    <hr/>
    <ul class="information-ul">
        <li>
            <div id="organizeTotal">
                <img src="${staticpath}/after/images/blue-total.png">


            </div>
            <h5>机构总数</h5>
        </li>
        <li>
            <div id="organizeA">
                <img src="${staticpath}/after/images/A.png">
            </div>
            <h5>A级机构</h5>
        </li>
        <li>
            <div id="organizeB">
                <img src="${staticpath}/after/images/B.png">
            </div>
            <h5>B级机构</h5>
        </li>
        <li>
            <div id="organizeC">
                <img src="${staticpath}/after/images/C.png">
            </div>
            <h5>C级机构</h5>
        </li>
    </ul>

</div>
<div class=" money-information">
    <div>
        <img src="${staticpath}/after/images/necktie.png" alt=""/>

        <h3>志愿者概况</h3>

    </div>
    <hr/>
    <ul class="information-ul">
        <li>
            <div id="accountTotal">
                <img src="${staticpath}/after/images/user.png" class="user-img">
            </div>
            <h5>志愿者总数</h5>
        </li>

    </ul>

</div>
<div class=" money-information">
    <div>
        <img src="${staticpath}/after/images/star.png" alt=""/>

        <h3>活动概况</h3>

    </div>
    <hr/>
    <ul class="information-ul">
        <li>
            <div id="offlineTotal">
                <img src="${staticpath}/after/images/total.png">
            </div>
            <h5>活动总数</h5>
        </li>
        <li>
            <div id="offlineStart">
                <img src="${staticpath}/after/images/transverse.png" class="circular-img">

            </div>
            <h5>未开始</h5>
        </li>
        <li>
            <div id="offlineHave">
                <img src="${staticpath}/after/images/spot.png" class="circular-img">
            </div>
            <h5>进行中</h5>
        </li>
        <li>
            <div id="offlineEnd">
                <img src="${staticpath}/after/images/fork.png" class="circular-img">
            </div>
            <h5>已结束</h5>
        </li>
    </ul>

</div>
<script>
    //查询机构总数
    function queryOrganize() {
        $.ajax({
            type: "GET",
            url: "${basepath}/rest/manage/organize/selectAllList",
            dataType: "json",
            data: {
                status: 1
            },
            success: function (data) {
                var organizeList = data.data;
                var organizeHtml = "";
                organizeHtml += '<h1>' + organizeList.length + '</h1>';
                document.getElementById("organizeTotal").innerHTML += organizeHtml;
            }
        })
    }
    queryOrganize();
    //查询A级机构
    function queryOrganizeA() {
        $.ajax({
            type: "GET",
            url: "${basepath}/rest/manage/organize/selectAllList",
            dataType: "json",
            data: {
                status: 1,
                rank: 1
            },
            success: function (data) {
                var organizeList = data.data;
                var organizeHtml = "";
                organizeHtml += '<h1>' + organizeList.length + '</h1>';
                document.getElementById("organizeA").innerHTML += organizeHtml;
            }
        })
    }
    queryOrganizeA();
    //查询B级机构
    function queryOrganizeB() {
        $.ajax({
            type: "GET",
            url: "${basepath}/rest/manage/organize/selectAllList",
            dataType: "json",
            data: {
                status: 1,
                rank: 2
            },
            success: function (data) {
                var organizeList = data.data;
                var organizeHtml = "";
                organizeHtml += '<h1>' + organizeList.length + '</h1>';
                document.getElementById("organizeB").innerHTML += organizeHtml;
            }
        })
    }
    queryOrganizeB();
    //查询C级机构
    function queryOrganizeC() {
        $.ajax({
            type: "GET",
            url: "${basepath}/rest/manage/organize/selectAllList",
            dataType: "json",
            data: {
                status: 1,
                rank: 3
            },
            success: function (data) {
                var organizeList = data.data;
                var organizeHtml = "";
                organizeHtml += '<h1>' + organizeList.length + '</h1>';
                document.getElementById("organizeC").innerHTML += organizeHtml;
            }
        })
    }
    queryOrganizeC();
    //查询志愿者总数
    function queryAccount() {
        $.ajax({
            type: "GET",
            url: "${basepath}/rest/manage/account/selectAllList",
            dataType: "json",
            data: {
                status: 1
            },
            success: function (data) {
                var accountList = data.data;
                var accountHtml = "";
                accountHtml += '<h1>' + accountList.length + '</h1>';
                document.getElementById("accountTotal").innerHTML += accountHtml;
            }
        })
    }
    queryAccount();
    //查询活动总数
    function queryOffline() {
        $.ajax({
            type: "GET",
            url: "${basepath} /manage/offline/selectAllList",
            dataType: "json",
            success: function (data) {
                var offlineList = data.data;
                var offlineHtml = "";
                offlineHtml += '<h1>' + offlineList.length + '</h1>';
                document.getElementById("offlineTotal").innerHTML += offlineHtml;
            }
        })
    }
    queryOffline();
    //查询活动未开始
    function queryOfflineStart() {
        $.ajax({
            type: "GET",
            url: "${basepath} /manage/offline/selectAllList",
            dataType: "json",
            data: {
                status: 2
            },
            success: function (data) {
                var offlineList = data.data;
                var offlineHtml = "";
                offlineHtml += '<h1>' + offlineList.length + '</h1>';
                document.getElementById("offlineStart").innerHTML += offlineHtml;
            }
        })
    }
    queryOfflineStart();
    //查询活动进行中
    function queryOfflineHave() {
        $.ajax({
            type: "GET",
            url: "${basepath} /manage/offline/selectAllList",
            dataType: "json",
            data: {
                status: 3
            },
            success: function (data) {
                var offlineList = data.data;
                var offlineHtml = "";
                offlineHtml += '<h1>' + offlineList.length + '</h1>';
                document.getElementById("offlineHave").innerHTML += offlineHtml;
            }
        })
    }
    queryOfflineHave();
    //查询活动已结束
    function queryOfflineEnd() {
        $.ajax({
            type: "GET",
            url: "${basepath} /manage/offline/selectAllList",
            dataType: "json",
            data: {
                status: 4
            },
            success: function (data) {
                var offlineList = data.data;
                var offlineHtml = "";
                offlineHtml += '<h1>' + offlineList.length + '</h1>';
                document.getElementById("offlineEnd").innerHTML += offlineHtml;
            }
        })
    }
    queryOfflineEnd();
</script>


</@page.pageBase>