<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="文章管理">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }

    .button-primary, .button-success, .button-danger {
        margin-left: 5px;
    }
</style>
<form id="searchForm" class="form-panel">
    <ul class="panel-content">
        <li>
            <div class="control-group span5">
                <div class="controls">
                    <input type="text" name="title" id="title" value="" placeholder="输入文章标题">
                </div>
            </div>
            <div class="form-actions span8">
                <button type="submit" class="button  button-primary">
                    搜索
                </button>
                <a href="${basepath}/rest/manage/news/toAdd" class="button button-success">新增</a>
                <button class="button button-danger" onclick="return delFunction()">
                    删除
                </button>
            </div>
        </li>
    </ul>
</form>
<div id="grid"></div>
<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: 'ID', dataIndex: 'id', width: '10%',elCls: 'center'},
                {title: '标题', dataIndex: 'title', width: "40%",elCls: 'center'},
                {title: '最后操作时间', dataIndex: 'updatetime', width: "20%",elCls: 'center'},

                {
                    title: '显示状态', dataIndex: 'status',elCls: 'center',width: "20%", renderer: function (data) {
                    if (data == "y") {
                        return '<img src="${basepath}/resource/images/action_check.gif">';
                    } else {
                        return '<img src="${basepath}/resource/images/action_delete.gif">';
                    }
                }
                },
                {
                    title: '操作', dataIndex: 'id',elCls: 'center', width: "20%", renderer: function (value) {
                    <#if checkPrivilege("/manage/news/edit")>
                        return '<a href="${basepath}/rest/manage/news/toEdit?id=' + value + '">编辑</a>';
                    <#else>
                        return "";
                    </#if>
                }
                }
            ];
    var store = new Store({
                url: 'loadData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    status: $("#status").val()
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',//统一格式
                totalProperty: 'total'//统一格式
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                plugins: [Grid.Plugins.CheckSelection], // 插件形式引入多选表格

                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });
    grid.render();

    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });

    //删除选中的记录
    function delFunction() {
        var selections = grid.getSelection();
        var ids = new Array();
        for (var i = 0; i < selections.length; i++) {
            ids[i] = selections[i].id.toString()
        }
        $.ajax({
            type: "POST",
            url: "${basepath}/rest/manage/news/deletesJson",//批量删除
            dataType: "json",
            data: {
                ids: ids
            },
            success: function (data) {
                var obj = form.serializeToObject();
                obj.start = 0; //返回第一页
                store.load(obj);
            }
        });
    }
</script>
</@page.pageBase>