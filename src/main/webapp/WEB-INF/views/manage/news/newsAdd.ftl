<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="文章管理">
<style>
    .button-large {
        font-size: 16px;
    }

    hr {
        border-color: #009688;
    }
</style>
<div class="xm-offline">
    <div class="row">
        <div class="panel">
            <div class="panel-header">
                <a href="${basepath}/rest/manage/news/toList">返回上一级</a>
            </div>
            <div class="panel-body">
            <#--form表单提交，相当于data-->
                <form id="addForm" class="form-horizontal" action="${basepath}/rest/manage/news/insertJson" method="post">
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            文章标题：
                        </label>

                        <div class="controls">
                            <input type="text" data-rules="{required:true}" class="control-text span-width span10"
                                   name="title" placeholder="请输入文章标题">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">显示状态：</label>

                        <div class="controls">
                            <select data-rules="{required:true}" name="status">
                                <option value="">-请选择-</option>
                                <option value="y">显示</option>
                                <option value="n">不显示</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            文章内容：
                        </label>

                        <div class="controls control-row12" style="height: 350px;initialFrameWidth: 100%;">
                            <textarea type="text" class="input-large" name="content" id="content"
                                      data-rules="{required:true}"></textarea>
                        </div>
                    </div>
                    <h3 class="offset2">文章图片
                        <a class="button button-primary pull-right" id="imagesBtn" style="height:20px">新增</a>
                    </h3>
                    <hr>
                    <input type="hidden" class="control-text span-width " name="pictures" id="picture">

                    <div id="imagesGrid" class="xm-grid">

                    </div>
                    <div class="centered">
                        <button type="submit" class="button  button-large  button-success">立即发布</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var editor = UE.getEditor('content');
    var Grid = BUI.Grid;
    var Store = BUI.Data.Store;
    var Data = BUI.Data;
    var Calendar = BUI.Calendar;
    var datePicker = new Calendar.DatePicker({
        trigger: '.calendar-time',
        showTime: true,
        autoRender: true
    });
    var addForm = new BUI.Form.Form({
        srcNode: '#addForm',
        submitType: 'ajax',
        callback: function (data) {
            if (data.success == true) {
                window.location.href = "${basepath}/rest/manage/news/toList"
            } else {

            }
        }
    }).render();

    /*--------------------------活动图片列表  begin--------------------*/
    /**
     * 图片信息
     * @type {*[]}
     */
    var imagesColumns = [
        {
            title: '图片', dataIndex: 'url', width: '30%', renderer: function (data) {
            return '<img src="${basepath}/' + data + '" width="100px" height="100px"/>'
        }
        },
        {
            title: '状态', dataIndex: 'state', width: '30%', renderer: function (data) {
            if (data == "SUCCESS") {
                return "上传成功";
            }
            return "上传失败";
        }
        },
        {
            title: '操作', dataIndex: 'title', width: '40%', renderer: function (data, obj, index) {
            return '<a href="javascript:delImages(' + index + ')">删除</a>';
        }
        }
    ];
    /**
     * 删除缓存图片信息
     * @param index
     */
    function delImages(index) {
        var record = imagesStore.findByIndex(index);
        imagesStore.remove(record);
        var imgStr = imagesStore.getResult();
        var imgArr = new Array();
        for (var i = 0; i < imgStr.length; i++) {
            imgArr[i] = imgStr[i].url
        }
        $("#picture").val(imgArr.join(","));
    }
    var imagesStore = new Store({});
    var imagesGrid = new Grid.Grid({
        render: '#imagesGrid',
        width: '100%',//如果表格使用百分比，这个属性一定要设置
        columns: imagesColumns,
        idField: 'title',
        store: imagesStore
    });
    imagesGrid.render();
    /*--------------------------活动图片列表  end--------------------*/

    /*------------------------图片上传插件配置  begin-----------------------*/
    var uploader = WebUploader.create({
        auto: true,
        swf: '${staticpath}/ueditor/third-party/webuploader/Uploader.swf',
        server: '${basepath}/rest/manage/ued/config?action=uploadimage',
        pick: '#imagesBtn',//绑定事件
        resize: false,
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/gif,image/jpg,image/jpeg,image/bmp,image/png,'
        }
    });

    uploader.on('uploadSuccess', function (file, response) {
        uploader.removeFile(file);//删除缓存
        imagesStore.add(response);//给store赋值
        var imgStr = imagesStore.getResult();

        //将数据源store转化成字符串赋值给input
        var imgArr = new Array();
        for (var i = 0; i < imgStr.length; i++) {
            imgArr[i] = imgStr[i].url
        }
        $("#picture").val(imgArr.join(","));
    });
    /*------------------------图片上传插件配置  end-----------------------*/
</script>

</@page.pageBase>