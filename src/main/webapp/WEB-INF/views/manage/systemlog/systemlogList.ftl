<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="日志管理">
<style>
    .form-horizontal .controls {
        line-height: 40px;
        height: 40px;
    }

    .button-primary, .button-success, .button-danger {
        margin-left: 5px;
    }

    .form-horizontal .time-label {
        width: 80px;
        line-height: 40px;
    }

    .form-horizontal .account-label {
        width: 70px;
        line-height: 40px;
    }

    .form-horizontal .time-controls {
        margin-left: 5px;
    }
    [class*="span"] {
        margin-left: 0px;
    }
</style>
<form id="searchForm" class="form-panel">
    <ul class="panel-content">
        <li>
            <div class="control-group span6">
                <label class="time-label control-label">是否异登陆：</label>

                <div class="time-controls  controls">
                    <#assign y_n = {'':"全部",'y':'是','n':'否'}>
                    <select name="diffAreaLogin" class="input-normal" id="diffAreaLogin">
                        <#list y_n?keys as key>
                            <option value="${key}"
                                    <#if e.diffAreaLogin?? && e.diffAreaLogin==key>selected="selected" </#if>>${y_n[key]}</option>
                        </#list>
                    </select>
                </div>
            </div>
            <label class="account-label control-label">登陆账号：</label>

            <div class="time-controls controls">
                <input type="text" value="${e.account!""}" class="input-normal" name="account" id="account"/>
            </div>
            <div class="control-group span8">
                <div class="form-actions">
                    <button type="submit" class="button button-primary">查询
                    </button>
                </div>
            </div>
        </li>
    </ul>
</form>
<div id="grid"></div>

<script>
    var Grid = BUI.Grid,
            Store = BUI.Data.Store,
            columns = [
                {title: '标题', dataIndex: 'title', width:'20%',elCls: 'center'},
                {title: '账号', dataIndex: 'account', width:'20%',elCls: 'center'},
                {title: '登陆时间', dataIndex: 'logintime', width:'20%',elCls: 'center'},
                {title: '登陆IP', dataIndex: 'loginIP', width:'10%',elCls: 'center'},
                {title: '登陆位置', dataIndex: 'loginArea', width:'20%',elCls: 'center'},
                {
                    title: '是否异地登录', dataIndex: 'diffAreaLogin', width:'10%',elCls: 'center', renderer: function (data) {
                    return data == "y" ? "是" : "否";
                }
                }
            ];

    var store = new Store({
                url: 'loadData',
                autoLoad: true, //自动加载数据
                params: { //配置初始请求的参数
                    length: '10',
                    diffAreaLogin: $("#diffAreaLogin").val(),
                    account: $("#account").val()
                },
                pageSize: 10,	// 配置分页数目
                root: 'list',
                totalProperty: 'total'
            }),
            grid = new Grid.Grid({
                render: '#grid',
                columns: columns,
                loadMask: true, //加载数据时显示屏蔽层
                store: store,
                // 底部工具栏
                bbar: {
                    pagingBar: true
                }
            });

    grid.render();

    var form = new BUI.Form.HForm({
        srcNode: '#searchForm'
    }).render();

    form.on('beforesubmit', function (ev) {
        //序列化成对象
        var obj = form.serializeToObject();
        obj.start = 0; //返回第一页
        store.load(obj);
        return false;
    });


</script>

</@page.pageBase>