<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="系统设置">
<style>
    .button-large {
        font-size: 16px;
    }
</style>
<div class="xm-offline">
    <div class="row">
        <div class="panel">
            <div class="panel-header">
                <strong>系统设置</strong>
            </div>
            <div class="panel-body">
                <form id="editForm" class="form-horizontal" action="${basepath}/rest/manage/systemSetting/updateJson"
                      method="post">
                    <input type="hidden" class="control-text span-width span10" name="id" value="${e.id!}">

                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            系统编码：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span10" name="systemCode"
                                   value="${e.systemCode!}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">系统名称：</label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span10" name="name" value="${e.name!}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">站点路径：</label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span10" name="www" value="${e.www!}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            系统logo：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span10" name="logo" value="${e.logo!}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            系统标题：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span10" name="title" value="${e.title!}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            系统描述：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span10" name="description"
                                   value="${e.description!}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            SEO关键字：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span10" name="keywords"
                                   value="${e.keywords!}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            页面ico：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span10" name="shortcuticon"
                                   value="${e.shortcuticon!}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            微信公众号ID：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span10" name="appid" value="${e.appid!}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">
                            <s>*</s>
                            微信公众号密钥：
                        </label>

                        <div class="controls">
                            <input type="text" class="control-text span-width span10" name="secret"
                                   value="${e.secret!}">
                        </div>
                    </div>
                    <div class="centered">
                        <button type="submit" class="button  button-large  button-success">保存设置</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var Grid = BUI.Grid;
    var Store = BUI.Data.Store;
    var imagesStore = new Store({});
    var editForm = new BUI.Form.Form({
        srcNode: '#editForm',
        submitType: 'ajax',
        callback: function (data) {
            if (data.success == true) {
                BUI.Message.Alert('设置成功', 'success');
            } else {
                BUI.Message.Alert('设置失败', 'error');
            }
        }
    }).render();
</script>

</@page.pageBase>