<#import "../tpl/pageTep.ftl" as page>
<@page.pageBase currentMenu="缓存更新">
<style>
.controls {
        line-height: 40px;
        height: 40px;
    }

    .button-primary {
        margin-left: 5px;
    }

     .time-label {
        float: left;
        width: 36px;
        line-height: 40px;
    }

   .time-controls {
        float: left;
        width: 126px;
        margin-left: 5px;
    }

    [class*="span"] {
        margin-left: 5px;
    }
</style>
<div class="form-panel">
    <ul class="panel-content">
        <li>
            <label class="time-label control-label">状态：</label>
            <div class="time-controls controls">
                <select name="status" class="input-normal" id="status">
                    <option value="loadAllCache">后台缓存</option>
                    <option value="frontCache">前台缓存</option>
                    <option value="wxTokenCache">微信Token缓存</option>
                    <option value="wxJsApiCache">微信JsApi缓存</option>
                </select>
            </div>
            <div class="form-actions span8">
                <button class="button  button-primary" onclick="return loadCache()">
                    立即更新
                </button>
            </div>
        </li>

    </ul>
</div>
<script>
    function loadCache() {
        $.ajax({
            url: '${basepath}/rest/manage/cache/' + $("#status").val(),
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                BUI.Message.Alert('更新成功！', 'success');
            }
        })
    }
</script>
</@page.pageBase>